# My digital invitation card

This is a webpage with the goal of acting as a digital invitation card for my upcoming birthday. to act as hub for my virtual birthday party.

I wanted to start a project like this and my birthday gave me the right opportunity. I decided against using a CMS like wordpress and instead used [Bootstrap](https://getbootstrap.com/) as a resource. This [tutorial](https://bootstrapious.com/p/how-to-build-a-working-bootstrap-contact-form) was a great asset and helped me a lot in regards to the contact form.
